// import { useRouter } from 'next/router'
 
// export default function Blog() {
//   const router = useRouter()
//   return <p>Post: {router.query.slug}</p>
// }
import client from '../../lib/client';
import { AiOutlineStar } from "@react-icons/all-files/Ai/AiOutlineStar";
import { AiOutlineMinus } from "@react-icons/all-files/Ai/AiOutlineMinus";
import { AiOutlinePlus } from "@react-icons/all-files/Ai/AiOutlinePlus";
import { AiFillStar } from "@react-icons/all-files/Ai/AiFillStar";
import { urlFor } from '../../lib/client';
import { Product } from '../../components';

// import {  } from '../lib/client';
import React, { useState } from 'react'
import Head from 'next/head';
// import { Product } from '@/components';

const ProductDetails = ({ products , product }) => {
const{image , name , details , price} = product;
const [index, setIndex] = useState(0)
  return (
    <div>
    <div className='row'>
    <div 
    className="product-details-container col-lg-6 col-12">
        <div>
          <div className='image-container'>
            <img src={urlFor(image && image[index])} alt="" />
            </div>
          <div 
          className='samll-images-container'>
          {image?.map((item, i) => ( <img src={urlFor(item)}  
          className={i == index ? 'small-image selected-image' : 'small-image' } 
          onMouseEnter={() => setIndex(i)} /> ))}
        </div> 
        </div> 
      </div>
      <div className='product-detail-desc col-lg-6 col-12'>
<h1>{name}</h1>
<div className="reviews">
  <div>
    <AiFillStar />
    <AiFillStar />
    <AiFillStar />
    <AiFillStar />
    {/* <AiFillStar /> */}
    <AiOutlineStar />
  </div>
  <p>(20)</p>
</div>
<h4> Details: </h4>
<p>{details}</p>
<p className='price'> ${price}</p>
<div className='quantity'>
  <h3>Quantity: </h3>
  <p className='quantity-desc'>
    <span className='minus' onClick=""><AiOutlineMinus/> </span>
    <span className='num' onClick="">0</span>
    <span className='plus' onClick=""><AiOutlinePlus/> </span>
  </p>
</div>
<div className='buttons'>
<button type='button' className='add-to-cart'>Add to cart</button>
{/* &nbsp; */}
<button type='button' className='buy-now'>Buy Now</button>
</div>
</div>
    </div>
    <div className='maylike-products-wrapper'>
    <h2>You may also like</h2>
    <div className='marquee'>
      <div className='maylike-products-container track'>
        {products.map((item) => (<Product key= {item._id} product ={item}/> ))}
      </div>
    </div>
    </div>
    
    </div>
  )
}


// ..............................................................
//this query only return all the current product details when we click on any product
export const getStaticPaths = async () => {
  const query = `*[_type == "product"]{
    slug {
      current   
    }}`;

    const products = await client.fetch(query);

    const paths = products.map((product) => ({
      params: {slug: product.slug.current}
    }));

    return{
      paths,
      fallback: 'blocking'
    }
}

export const getStaticProps = async({params: {slug}}) => {
  // const client = createClient({
  //   projectId: 'zmdwfohj',
  //   dataset: 'production',
  //   useCdn: false
  // })
  const query = `*[_type == "product" && slug.current == '${slug}'][0]`;
  const productQuery = `*[_type == "product"]`;
  const product = await client.fetch(query);
  const products = await client.fetch(productQuery);

  console.log(product);

  // const bannerQuery = `*[_type == "banner"]`;
  // const bannerData = await client.fetch(bannerQuery)
  return{
    props: { products , product }
  }
}

export default ProductDetails;

   



