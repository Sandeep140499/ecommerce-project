import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
// import styles from '@/styles/Home.module.css';
import { createClient } from 'next-sanity';

import { Product, FooterBanner , HeroBanner } from '@/components/Index';

const inter = Inter({ subsets: ['latin'] })

export default function Home({products , bannerData }) {

  return (
    <>
    <Head>
    <title>ECommerce Shopping Website</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous"></link>
    </Head>

    <HeroBanner  
    heroBanner = {bannerData.length && bannerData[0]}/>


    <div className='products-heading'>
    <h2 >Best selling products</h2>
      <p>Speakers there are many variations passages </p>
    </div>
    <div className='products-container'>
     {products?.map((product)=>  <Product key={product._id} product={product}/> )}
    </div>
    <FooterBanner FooterBanner={bannerData && bannerData[0]}/>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
     </>
 
  )
}



export async function getServerSideProps(context){
  const client = createClient({
    projectId: 'zmdwfohj',
    dataset: 'production',
    useCdn: false
  })
  const query = `*[_type == "product"]`;
  const products = await client.fetch(query)

  const bannerQuery = `*[_type == "banner"]`;
  const bannerData = await client.fetch(bannerQuery)
  return{
    props: { products , bannerData }
  }
}
