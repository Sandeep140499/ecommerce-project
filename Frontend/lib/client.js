import SanityClient  from "@sanity/client";

import ImageUrlBuilder  from "@sanity/image-url";


// const SanityClient = require('@sanity/client');

const client =  SanityClient({
    projectId: 'zmdwfohj',
    dataset: 'production',
    apiVersion: '2023-07-18',
    useCdn: false,
    token: process.env.NEXT_PUBLIC_SANITY_TOKEN,
});

export default client

const builder = ImageUrlBuilder(client);

export const urlFor = (source) => builder.image(source);

