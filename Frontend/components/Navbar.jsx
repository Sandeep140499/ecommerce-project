import React from 'react'
import Link from 'next/link';
import Head from 'next/head';
import { AiOutlineShopping } from "@react-icons/all-files/Ai/AiOutlineShopping"; ////import icons to install packages

const Navbar = () => {
  return (
    <>
    <Head>
    <title>ECommerce Shopping Website</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous"></link>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    </Head>
    <div className='navbar-conatiner '>
<div className='row p-lg-2'>
  <div className="col-12 col-lg-6 logo">
  <Link href="/">
    {/* SKC-Product */}
        <div className='row '>
          <div className='col-12 col-lg-4 logo'>
            <div className='col-auto'>
            <span><i class="fa-solid fa-s fa-bounce fa-2xl"></i></span>
            <span><i class="fa-solid fa-h fa-bounce fa-2xl"></i></span>
            <span><i class="fa-solid fa-o fa-bounce fa-2xl"></i></span>
            <span><i class="fa-solid fa-p fa-bounce fa-2xl"></i></span>
            <span><i class="fa-solid fa-p fa-bounce fa-2xl"></i></span>
            <span><i class="fa-solid fa-i fa-bounce fa-2xl"></i></span>
            <span><i class="fa-solid fa-n fa-bounce fa-2xl"></i></span>
            <span><i class="fa-solid fa-g fa-bounce fa-2xl"></i></span>
            </div>
          </div>
        </div>
      </Link>
  </div>
  <div className="col-12 col-lg-6 text-end">
  <button type='button' className='cart-icon' onClick="">
        <AiOutlineShopping />
        <span className='cart-item-qty'>1</span>
      </button>
  </div>
</div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
    </>
    
  )
}

export default Navbar;