import React from 'react'
import Link from 'next/link';

import  {urlFor}  from '../lib/client';
import Head from 'next/head';

const HeroBanner = ({heroBanner}) => {
  return (
   <>
   <Head>
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
   </Head>
     <div className='hero-banner-container'>
     
     <div>
     
         <p className='beats-solo'> {heroBanner.smallText}</p>
         <br/>
         <h2>{heroBanner.saleTime}</h2>
         {/* <h2>{heroBanner.discount}</h2> */}
    
         <h3>{heroBanner.midText}</h3>
         {/* <h1>{heroBanner.largeText1}</h1> */}
         <h1>{heroBanner.largeText2}</h1>
         <img src ={urlFor(heroBanner.image)} alt="headphones" className='hero-banner-image' />
     </div>
   <div>
 
   </div>
     <div>
  
        <div>
        
        <Link href="{`/product/${heroBanner.product}`}">
 
 <button type='button'>{heroBanner.buttonText}</button>
 </Link>
        </div>
 
 
         <div className='desc'>
             <h5>Description</h5>
             <p>{heroBanner.desc}</p>
         </div>
     </div>
 
     </div>
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
   </>
  )
}

export default HeroBanner;