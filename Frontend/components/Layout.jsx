import React from 'react'
import Head from 'next/head';
import Navbar  from '../components/Navbar';
// import header  from 'next/dist/lib/load-custom-routes';
import Footer from './Footer';

const Layout = ({children}) => {
  return (
   <div className='layout'>
       <Head>
    <title>ECommerce Shopping Website</title>
    </Head>

    <header>
    <Navbar />
    </header>

    <main className='main-container'>
   {children}
    </main>

    <footer>
      <Footer />
    </footer>

   </div>
  )
}

export default Layout;