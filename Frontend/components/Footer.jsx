import React from 'react'
import { AiFillInstagram} from "@react-icons/all-files/Ai/AiFillInstagram";
import {  AiFillTwitterCircle } from "@react-icons/all-files/Ai/AiFillTwitterCircle";

const Footer = () => {
  return (
    <div className='footer-container'>
      <p>2023 SKC-Production All rights reserved</p>
      <p className='icons'>
      <a href=''>  <AiFillInstagram /></a>
        <a href=''><AiFillTwitterCircle /></a>
      </p>
    </div>
  )
}

export default Footer;